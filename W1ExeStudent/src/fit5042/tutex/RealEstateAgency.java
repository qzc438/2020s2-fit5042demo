package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author qzc438
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;
    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    public void createRepositories() throws Exception{
        propertyRepository.addProperty(new Property(1, "24 Boston Ave Malvern East VIC 3145", 150, 420000));
        propertyRepository.addProperty(new Property(2, "11 Bettina St, Clayton VIC 3168, Australia", 352, 360000.00));
        propertyRepository.addProperty(new Property(3, "3 Wattle Glen Huntly VIC 3163, Australia", 800, 650000.00));
        propertyRepository.addProperty(new Property(4, "3 Hamilton St, Bentleigh VIC 3204, Australia", 170, 435000.00));
        propertyRepository.addProperty(new Property(5, "82 Spring RD, Hampton East VIC 3188, Australia", 60, 820000.00));
        System.out.println("5 properties added successfully");
    }
    
    public void displayAllProperties() throws Exception{
        List<Property> li = propertyRepository.getAllProperties();
        for(Property p : li){
            System.out.println(p.getId()+" " + p.getAddress() + " " + p.getSize() + "sqm"+ " $" + p.getPrice());
        }
    }
    
    public void run() throws Exception{
        createRepositories();
        displayAllProperties();
        System.out.print("Please enter the ID of ther property you want to search: ");
        Scanner console = new Scanner(System.in);
        int number = Integer.parseInt(console.nextLine());
        Property p = propertyRepository.searchPropertyById(number);
        System.out.println(p.getId()+" " + p.getAddress() +" " + p.getSize() + "sqm"+ " $" + p.getPrice());
    }
    
    public static void main(String args[]){
        try {
            new RealEstateAgency("Wilson").run();
        } catch (Exception ex) {
            Logger.getLogger(RealEstateAgency.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
